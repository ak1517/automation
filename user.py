from registry_setup import registry
import pandas as pd
from pipeline import visualise,preProcess,process
from pipeline.mainClass import mainClass
import nbformat as nbf
from os import listdir
from os.path import isfile, join
from cmapPy.pandasGEXpress.parse import parse

registry_keys = registry().get_registry_keys() 
class generator():

    def getRScripts(self,mypath):
        a = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        b=[]
        for x in a:
            b.append(x.split('.'))
        c=[]
        for i in range(len(b)):
            if b[i][1]=='R':
                c.append(b[i][0])
        return c

    def getArgsPython(self,x):
        s=[]
        for key,values in x.items():
            if key!="perform":
                s.append(key+" = "+str(values))
        a="mainObject"
        for i in range(len(s)):
            a+=","+s[i]
        return a

    def putPython(self,x):
        s=[]
        for key,values in x.items():
            if key!="perform":
                s.append(key+" = "+str(values))
        a="df_RPy = mainObject.df"
        for i in range(len(s)):
            a+=","+s[i]
        return a

    def getPython(self,x):
        a="""\
try:
    inPlace = params["""+x+"""]['inPlace']
except:
    inPlace = False
if inPlace:
    mainObject.df = df_RPy"""
        return a

    def getArgsR(self,x):
        s=[]
        for key,values in x.items():
            if key!="perform":
                s.append(key+" = "+key)
        a="df_RPy"
        for i in range(len(s)):
            a+=","+s[i]
        return a

    def putR(self):
        a="%put df_RPy\n"
        return a

    def getR(self,x):
        s=[]
        for key,values in x.items():
            if key!="perform":
                s.append(key)
        a="%get df_RPy"
        for i in range(len(s)):
            a+=", "+s[i]
        a+="\n"
        return a

    def getImportStatements(self):
        code="""\
from pipeline import preProcess, visualise, process, workflows
from pipleline.mainClass import mainClass"""
        return code

    def getMainObject(self):
        code="""\
mainObject=mainClass()
df=automate().getDF()
mainObject.setDF(df)
rmd=automate.getRMD()
mainObject.setRMD(rmd)
cmd=automate().getCMD()
mainObject.setCMD(cmd)"""
        return code

    def generateNotebook(self,parent_list,apply,params,rScripts):
        nb=nbf.v4.new_notebook()
        nb['metadata']={
        "kernelspec": {
            "display_name": "SoS",
            "language": "sos",
            "name": "sos"
            },
            "language_info": {
            "codemirror_mode": "sos",
            "file_extension": ".sos",
            "mimetype": "text/x-sos",
            "name": "sos",
            "nbconvert_exporter": "sos_notebook.converter.SoS_Exporter",
            "pygments_lexer": "sos"
        }
        }
        cells=[]
        cells.append(nbf.v4.new_code_cell(self.getImportStatements()))
        cells.append(nbf.v4.new_code_cell(self.getMainObject()))
        for i in range(len(apply)):
            if apply[i] in rScripts:
                code=self.putPython(params[apply[i]])
                cells.append(nbf.v4.new_code_cell(code))
                code=self.putR()+self.getR(params[apply[i]])
                code+="source("+apply[i]+".R)"+"\n"
                code+="perform_"+apply[i]+"("+self.getArgsR(params[apply[i]])+")"
                cell=nbf.v4.new_code_cell(code)
                cell['metadata']={"kernel": "R"}
                cells.append(cell)  
                code=self.getPython(apply[i])
                cell=nbf.v4.new_code_cell(code)
                cell['metadata']={"kernel": "SoS"}
                cells.append(cell)                  
            else:
                code=parent_list[i]+"."+apply[i]+"."+parent_list[i]+"("+self.getArgsPython(params[apply[i]])+")"
                cells.append(nbf.v4.new_code_cell(code))
                
        nb['cells']=cells
        nbf.write(nb,'test3.ipynb')


class automate():
    
    def __init__(self,df,apply,params,notebook=None,rScripts=None):
        self.get_data(df)
        parent_list = self.find_parent_list(apply)
        if notebook:
            self.generateNotebook(parent_list,apply,params,rScripts)
        else:
            self.perform(apply,parent_list,params)
    
    def perform(self,apply,parent_list,params):        
        for i in range(len(apply)):
            x = getattr(getattr(__import__("pipeline"),parent_list[i]), apply[i])
            y = getattr(x,parent_list[i])
            y(**params[apply[i]])
            
    
    def find_parent_list(self,apply):
        list2=[]
        for x in apply:
            for key,values in registry_keys.items():
                if x in values:
                    list2.append(key)
        return list2

    def get_data(self,df):
        if isinstance(df,str):
            extension = df.split('.')[-1]
            if(extension == 'csv'):
                data = pd.read_csv(df)
                mainClass.df = data
            elif(extension == 'gct'):
                x = parse(df)
                mainClass.df = x.data_df
                mainClass.rowMetaData = x.row_metadata_df
                mainClass.colMetaData = x.col_metadata_df
            else:
                raise ValueError('Sorry file format is not defined.')
        else:
             mainClass.df = df 
    
    def getStr(self,x):
        s=[]
        for key,values in x.items():
            s.append(key+"="+str(values))
        if len(s)==1:
            return s[0]
        a=s[0]
        for i in range(1,len(s)):
            a+=","+s[i]
        return a

    def generateNotebook(self,parent_list,apply,params,rScripts):
        nb=nbf.v4.new_notebook()
        nb['metadata']={
        "kernelspec": {
            "display_name": "SoS",
            "language": "sos",
            "name": "sos"
            },
            "language_info": {
            "codemirror_mode": "sos",
            "file_extension": ".sos",
            "mimetype": "text/x-sos",
            "name": "sos",
            "nbconvert_exporter": "sos_notebook.converter.SoS_Exporter",
            "pygments_lexer": "sos"
        }
        }
        cells=[]
        for i in range(len(apply)):
            code=parent_list[i]+"."+apply[i]+"."+parent_list[i]+"("+self.getStr(params[apply[i]])+")"
            cells.append(nbf.v4.new_code_cell(code))
        #nb['cells']=[nbf.v4.new_code_cell(code)]
        nb['cells']=cells
        nbf.write(nb,'test1.ipynb')


#automate('output.csv',apply=["boxPlot","pca"],nComp=2)

# params=registry().getDictFromYaml('../data/parameters.yaml')
# apply=registry().getApply(params)
# automate("data/labeled_LCMS_total_labels.gct",apply=["limma"])

mainClass.colMetaData=pd.read_csv('data/meta_data.csv')
params=registry().getDictFromYaml('data/parameters.yaml')
apply=registry().getApply(params)
# print(apply)
# print(params[apply[0]])
automate('data/main_data.csv',apply=apply,params=params)