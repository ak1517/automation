from pipeline import visualise,preProcess,process,workflows
import inspect
import yaml

class registry:

    def __init__(self):
        self.registry_keys = []
        self.create_registry_keys()     


    def create_registry_keys(self):
        #-----create registry keys------------
        ppList=[]
        vList=[]
        pList=[]
        wList=[]

        for name,obj in inspect.getmembers(preProcess):
            if inspect.isclass(obj):
                clname = str(obj).split("'")[1]
                if clname.startswith("pipeline"):
                    ppList.append(name)     

        for name,obj in inspect.getmembers(visualise):
            if inspect.isclass(obj):
                clname = str(obj).split("'")[1]
                if clname.startswith("pipeline"):
                    vList.append(name)

        for name,obj in inspect.getmembers(process):
            if inspect.isclass(obj):
                clname = str(obj).split("'")[1]
                if clname.startswith("pipeline"):
                    pList.append(name)
        for name,obj in inspect.getmembers(workflows):
            if inspect.isclass(obj):
                clname = str(obj).split("'")[1]
                if clname.startswith("pipeline"):
                    wList.append(name)
        #----assigning values to keys in the registry ---- 
        self.registry_keys={
            "preProcess":ppList,
            "visualise":vList,
            "process":pList,
            "workflows":wList
        }
        
    def get_registry_keys(self):
        return self.registry_keys

    def getApply(self,yamlDict):
        a=[]
        for key,values in yamlDict.items():
            take=False
            for key2,values2 in values.items():
                if values2 is not None:
                    take=True
            if take:
                a.append(key)
        return a
    
    def getDictFromYaml(self,fileAddress):
        return yaml.load(open(fileAddress))